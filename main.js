function validate(){
    let pass = document.querySelector('.pass');
    let passRepeat = document.querySelector('.pass-repeat');
    let text = document.createElement('p');
    let submit = document.querySelector('.password-form');
    submit.addEventListener('submit', () => {
        if (pass.value === passRepeat.value){
            alert("You are welcome");
        } else {
            passRepeat.after(text);
            text.innerText = 'Нужно ввести одинаковые значения.';
            text.style.color = 'red';
        }
    });
}
validate();

let icon1 = document.querySelector('.icon-password');
let icon2 = document.getElementById('icon2');

icon1.addEventListener('click', () => {
    var input = document.getElementById('input-pass');
    if (input.getAttribute('type') == 'password') {
        icon1.classList.toggle('fa-eye-slash');
        input.setAttribute('type', 'text'); 
    } else {
        icon1.classList.remove('fa-eye-slash');
        input.setAttribute('type', 'password');
        icon1.classList.add('fa-eye');
    }
});


icon2.addEventListener('click', () => {
    var input = document.getElementById('input-pass-repeat');
    if (input.getAttribute('type') == 'password') {
        icon2.classList.toggle('fa-eye-slash');
        input.setAttribute('type', 'text'); 
    } else {
        input.setAttribute('type', 'password');
        icon2.classList.remove('fa-eye-slash');
        icon2.classList.add('fa-eye');
    }
});